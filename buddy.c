/**********************************************************************
 * Copyright (c) 2018
 *	Sang-Hoon Kim <sanghoonkim@ajou.ac.kr>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 **********************************************************************/
#define TRUE	1
#define FALSE	0

#define CHK_NO_NEED_TO_BREAK		0
#define CHK_ALL_ELEMS_ALLOCATED		1
#define CHK_LIST_EMPTY				2


#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include <sys/queue.h>
#include "config.h"

/**
 * This is a element of chunk_list.
 * 
 * entries:		pointer of prev/next chunks.
 * start:		Start address of chunk.
 * order:		Order of chunk.
 * isAlloc
 * 				ALLOCATED: 		1
 * 				NOTALLOCATED:	0
 */
struct chunk {
	TAILQ_ENTRY(chunk) entries;

	unsigned int start;
	unsigned int order;
	unsigned int isAlloc;
};


/**
 * This is a list that has free chunks.
 * This is implemented by using TAILQ, and works like FIFO queue.
 * 
 * listHead: 	Head of chunk_list.
 * order:		Order of chunk_list.
 */
struct chunk_list {
	TAILQ_HEAD(, chunk) listHead;

	unsigned int order;
};


/**
 * NR_ORDERS = MAX_ORDER + 1
 * MAKE SURE my buddy implementation can handle
 * order-0 to order-@MAX_ORDER pages.
 */
struct buddy {
	struct chunk_list chunks[NR_ORDERS];
};


/**
 * This is your buddy system allocator instance!
 */
static struct buddy buddy;


/**
 * #Works like FIFO. Using oldest free-elements of free-list.
 * Find allocatable order of free-list.
 * Return Number:	return order of free-list.
 * Return -1:		Cannot ALLOC. Need to SPLIT or ENOMEM.
 */
int find_allocatable_order(const unsigned int order){
	if (order > MAX_ORDER){
		/* Cannot ALLOC at any orders. */		
		return -1;
	}

	if (!TAILQ_EMPTY(&(buddy.chunks[order].listHead))){
		struct chunk *ptr = TAILQ_FIRST(&(buddy.chunks[order].listHead));
		while(ptr != NULL){
			if (ptr->isAlloc == FALSE){
				/* Can ALLOC at this order. No need to SPLIT. */
				return order;
			}
			ptr = ptr->entries.tqe_next;
		}
	}
	/* Cannot ALLOC at this order. Need to SPLIT. */
	return find_allocatable_order(order + 1);
}


/**
 * #Works like FIFO. Using oldest free-elements of free-list.
 * Find allocatable free-chunk of @order-free-list.
 * Return *chunk:	return pointer of alloactable free-chunk.	 
 * Return NULL:		Cannot find allocatable free-chunk. Something Wrong.
 */
struct chunk *find_allocatable_chunk(const unsigned int order){
	struct chunk *ptr = TAILQ_FIRST(&(buddy.chunks[order].listHead));
	while(ptr != NULL){
		if (ptr->isAlloc == FALSE){
			return ptr;
		}
		ptr = ptr->entries.tqe_next;
	}
	perror("ERROR at find_allcoatable_chunk()");
	exit(1);
}

/**
 *    Your buddy system allocator should manage from order-0 to
 *  order-@MAX_ORDER. In the following example, assume your buddy system
 *  manages page 0 to 0x1F (0 -- 31, thus @nr_pages is 32) and pages from
 *  20 to 23 and 28 (0x14 - 0x17, 0x1C) are allocated by alloc_pages()
 *  by some orders.
 *  At this moment, the buddy system will split the address space into;
 *
 *      0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
 * 0x00 <-------------------4-------------------------->
 * 0x10 <----2----->X  X  X  X  <-----2---->X  <0><-1-->
 *                  ^  ^  ^  ^              ^
 *                  |--|--|--|--------------|
 *                     allocated
 *
 *   Thus, the buddy system will maintain the free chunk lists like;
 *
 * Order     | Start addresses of free chunks
 * ----------------------------------------------
 * MAX_ORDER |
 *    ...    |
 *     4     | 0x00
 *     3     |
 *     2     | 0x10, 0x18
 *     1     | 0x1e
 *     0     | 0x1d
 */


/**
 * Allocate 2^@order contiguous pages.
 *
 * Description:
 *    For example, when @order=0, allocate a single page, @order=2 means
 *  to allocate 4 consecutive pages, and so forth.
 *    From the example state above, alloc_pages(2) gives 0x10 through @*page
 *  and the corresponding entry is removed from the free chunk. NOTE THAT the
 *  free chunk lists should be maintained as 'FIFO' so alloc_pages(2) returns 
 *  0x10, not 0x18. 
 *    To hanle alloc_pages(3), the order-4 chunk (0x00 -- 0x0f) should
 *  be broken into smaller chunks (say 0x00 -- 0x07, 0x08 -- 0x0f), and
 *  the LEFT BUDDY will be returned through @page whereas RIGHT BUDDY
 *  will be put into the order-3 free chunk list.
 *
 * Return:
 *   0      : On successful allocation. @*page will contain the starting
 *            page number of the allocated chunk
 *  -EINVAL : When @order < 0 or @order > MAX_ORDER
 *  -ENOMEM : When order-@order contiguous chunk is not available in the system
 */
int alloc_pages(unsigned int *page, const unsigned int order)
{
	/**
	 * Your implementation will look (but not limited to) like;
	 *
	 * Check whether a chunk is available from chunk_list of @order
	 * if (exist) {
	 *    allocate the chunk from the list; Done!
	 * } else {
	 *    Make an order-@order chunk by breaking a higher-order chunk(s)
	 *    - Find the smallest free chunk that can satisfy the request
	 *    - Break the LEFT chunk until it is small enough
	 *    - Put remainders into the free chunk list
	 *
	 *    Return the allocated chunk via @*page
	 * }
	 *
	 *----------------------------------------------------------------------
	 * Print out below message using PRINTF upon each events. Note it is
	 * possible for multiple events to be happened to handle a single
	 * alloc_pages(). Also, MAKE SURE TO USE 'PRINTF', _NOT_ printf, otherwise
	 * the grading procedure will fail.
	 *
	 * - Split an order-@x chunk starting from @page into @left and @right:
	 *   PRINTF("SPLIT 0x%x:%u -> 0x%x:%u + 0x%x:%u\n",
	 *			page, x, left, x-1, right, x-1);
	 *
	 * - Put an order-@x chunk starting from @page into the free list:
	 *   PRINTF("PUT   0x%x:%u\n", page, x);
	 *
	 * - Allocate an order-@x chunk starting from @page for serving the request:
	 *   PRINTF("ALLOC 0x%x:%x\n", page, x);
	 *
	 * Example: A order-4 chunk starting from 0 is split into 0:3 and 8:3,
	 * and 0:3 is split again to 0:2 and 4:2 to serve an order-2 allocation.
	 * And then 0:2 is allocated:
	 *
	 * SPLIT 0x0:4 -> 0x0:3 + 0x8:3
	 * PUT   0x8:3
	 * SPLIT 0x0:3 -> 0x0:2 + 0x4:2
	 * PUT   0x4:2
	 * ALLOC 0x0:2
	 *
	 *       OR
	 *
	 * SPLIT 0x0:4 -> 0x0:3 + 0x8:3
	 * SPLIT 0x0:3 -> 0x0:2 + 0x4:2
	 * PUT   0x8:3
	 * PUT   0x4:2
	 * ALLOC 0x0:2
	 *
	 *       OR
	 *
	 * SPLIT 0x0:4 -> 0x0:3 + 0x8:3
	 * SPLIT 0x0:3 -> 0x0:2 + 0x4:2
	 * PUT   0x4:2
	 * PUT   0x8:3
	 * ALLOC 0x0:2
	 *----------------------------------------------------------------------
	 */

	if (order > MAX_ORDER || order < 0){
		return -EINVAL;
	}

	struct chunk *tar = NULL;
	int order_alloactable = find_allocatable_order(order);
	if (order_alloactable == -1){
		/**
		 * Cannot Allocate at current status.
		 * No matter what I do, I cannot allocate chunk at current status.
		 */
		return -ENOMEM;
	}
	else if (order_alloactable == order){
		/**
		 * Can ALLOC at current order(= @order).
		 * Just ALLOC.
		 */
		tar = find_allocatable_chunk(order_alloactable);

		struct chunk *new = (struct chunk *)malloc(sizeof(struct chunk));
		new->isAlloc = TRUE;
		new->order = tar->order;
		new->start = tar->start;

		/* FIFO Implement. */
		TAILQ_REMOVE(&(buddy.chunks[order_alloactable].listHead), tar, entries);
		free(tar);

		TAILQ_INSERT_TAIL(&(buddy.chunks[order_alloactable].listHead), new, entries);	
		*page = new->start;
		PRINTF("ALLOC 0x%x:%u\n"
			, new->start, new->order);
		return 0;
	}
	else{
		/**
		 * Can ALLOC at other orders.
		 * Need to SPLIT chunk of @order_alloactable-free list.
		 */
		int count = 0;
		int order_current = order_alloactable;
		struct chunk *left = NULL;
		while(order_current != order) {
			if (count == 0){
				tar = find_allocatable_chunk(order_current);
			}
			else{
				tar = left;
			}
			order_current = tar->order - 1;

			struct chunk *new1 = (struct chunk *)malloc(sizeof(struct chunk));
			new1->start = tar->start;
			new1->order = order_current;
			new1->isAlloc = FALSE;

			struct chunk *new2 = (struct chunk *)malloc(sizeof(struct chunk));
			new2->start = new1->start + (1 << new1->order);
			new2->order = new1->order;
			new2->isAlloc = FALSE;
			
			PRINTF("SPLIT 0x%x:%u -> 0x%x:%u + 0x%x:%u\n"
				, tar->start, tar->order
				, new1->start, new1->order
				, new2->start, new2->order);

			TAILQ_INSERT_TAIL(&(buddy.chunks[new1->order].listHead)
				, new1, entries);
			TAILQ_INSERT_TAIL(&(buddy.chunks[new2->order].listHead)
				, new2, entries);
			
			PRINTF("PUT   0x%x:%u\n"
				, new2->start, new2->order);

			TAILQ_REMOVE(&(buddy.chunks[tar->order].listHead)
				, tar, entries);
			free(tar);
			
			left = new1;
			count++;
		}

		/* FIFO Implement. */
		TAILQ_REMOVE(&(buddy.chunks[left->order].listHead)
			, left, entries);

		left->isAlloc = TRUE;
		TAILQ_INSERT_TAIL(&(buddy.chunks[left->order].listHead)
			, left, entries);
		*page = left->start;

		PRINTF("ALLOC 0x%x:%u\n"
			, left->start, left->order);

		return 0;
	}
	return -ENOMEM;
}


/**
 * 	Find Buddy of given @page.
 * 		Buddy O:	return firstPTR->start.
 * 					need to merge(firstPTR, secondPTR).
 * 		Buddy X:	return -1;
 */
int findBuddy(unsigned int page, unsigned int order
	, struct chunk **firstPTR, struct chunk **secondPTR){
	if (TAILQ_EMPTY(&(buddy.chunks[order].listHead))){
		perror("ERROR at findBuddy()");
		exit(1);
	}

	struct chunk *pageFinder = TAILQ_FIRST(&(buddy.chunks[order].listHead));

	struct chunk *firPtr = NULL;
	struct chunk *secPtr = NULL;

	int divMax = 1 << (order + 1);
	int divmin = 1 << order;

	while(pageFinder != NULL){
		if (pageFinder->start == page && pageFinder->isAlloc == FALSE){
			break;
		}
		pageFinder = pageFinder->entries.tqe_next;
	}

	if (pageFinder->start != page && pageFinder->isAlloc != FALSE){
		perror("ERROR at findBuddy()");
		exit(1);
	}

	struct chunk *buddyFinder = TAILQ_FIRST(&(buddy.chunks[order].listHead));
	while(buddyFinder != NULL){
		if (pageFinder->start > buddyFinder->start){
			/* CASE: 	buddyFinder	->	pageFinder */
			if (
				(pageFinder->start - buddyFinder->start == divmin) &&
				(pageFinder->isAlloc == FALSE) &&
				(buddyFinder->isAlloc == FALSE) &&
				(buddyFinder->start % divMax == 0)
				){
				firPtr = buddyFinder;
				secPtr = pageFinder; 
				break;
			}
		}
		else if (buddyFinder->start > pageFinder->start){
			/* CASE:	pageFinder -> buddyFinder */
			if (
				(buddyFinder->start - pageFinder->start == divmin) &&
				(buddyFinder->isAlloc == FALSE) &&
				(pageFinder->isAlloc == FALSE) &&
				(pageFinder->start % divMax == 0)
				){
				firPtr = pageFinder;
				secPtr = buddyFinder;
				break;
			}

		}
		buddyFinder = buddyFinder->entries.tqe_next;
	}

	if (firPtr != NULL && secPtr != NULL){
		*firstPTR = firPtr;
		*secondPTR = secPtr;
		return firPtr->start;
	}
	return -1;
}

/**
 * Free @page which are contiguous for 2^@order pages
 *
 * Description:
 *    Assume @page was allocated by alloc_pages(@order) above. 
 *  WARNING: When handling free chunks, put them into the free chunk list
 *  carefully so that free chunk lists work in FIFO.
 */
void free_pages(unsigned int page, const unsigned int order)
{
	/**
	 * Your implementation will look (but not limited to) like;
	 *
	 * Find the buddy chunk from this @order.
	 * if (buddy does not exist in this order-@order free list) {
	 *    put into the TAIL of this chunk list. Problem solved!!!
	 * } else {
	 *    Merge with the buddy
	 *    Promote the merged chunk into the higher-order chunk list
	 *
	 *    Consider the cascading case as well; in the higher-order list, there
	 *    might exist its buddy again, and again, again, ....
	 * }
	 *
	 *----------------------------------------------------------------------
	 * Similar to alloc_pages() above, print following messages using PRINTF
	 * when the event happens;
	 *
	 * - Merge order-$x buddies starting from $left and $right:
	 *   PRINTF("MERGE : 0x%x:%u + 0x%x:%u -> 0x%x:%u\n",
	 *			left, x, right, x, left, x+1);
	 *
	 * - Put an order-@x chunk starting from @page into the free list:
	 *   PRINTF("PUT  : 0x%x:%u\n", page, x);
	 *
	 * Example: Two buddies 0:2 and 4:2 (0:2 indicates an order-2 chunk
	 * starting from 0) are merged to 0:3, and it is merged again with 8:3,
	 * producing 0:4. And then finally the chunk is put into the order-4 free
	 * chunk list:
	 *
	 * MERGE : 0x0:2 + 0x4:2 -> 0x0:3
	 * MERGE : 0x0:3 + 0x8:3 -> 0x0:4
	 * PUT   : 0x0:4
	 *----------------------------------------------------------------------
	 */

	/* 1st. Find @start-chunk of @order-free-list. */
	struct chunk *finder = TAILQ_FIRST(&(buddy.chunks[order].listHead));
	while(finder != NULL){
		if (finder->start == page){
			/* FIFO Implement. */
			TAILQ_REMOVE(&(buddy.chunks[order].listHead), finder, entries);
			finder->isAlloc = FALSE;
			TAILQ_INSERT_TAIL(&(buddy.chunks[order].listHead), finder, entries);
			break;
		}
		finder = finder->entries.tqe_next;
	}

	/* 2nd. If @order == MAX_ORDER, END. */
	if (order == MAX_ORDER){
		return;
	}

	/* 3rd. Find buddy of given @page. */
	struct chunk *firstPTR = NULL;
	struct chunk *secondPTR = NULL;
	int isBuddy = findBuddy(page, order, &firstPTR, &secondPTR);

	if (isBuddy == -1){
		/* Cannot find buudy, END. */
		return;
	}
	else{
		/* Can find buddy, Need to MERGE(firstPTR, secondPTR). */
		struct chunk *merged = (struct chunk *)malloc(sizeof(struct chunk));
		merged->start = isBuddy;
		merged->order = order + 1;
		merged->isAlloc = FALSE;

		PRINTF("MERGE 0x%x:%u + 0x%x:%u -> 0x%x:%u\n"
		, firstPTR->start, firstPTR->order
		, secondPTR->start, secondPTR->order
		, merged->start, merged->order);

		TAILQ_INSERT_TAIL(&(buddy.chunks[merged->order].listHead)
			, merged, entries);
		PRINTF("PUT   0x%x:%u\n"
			, merged->start, merged->order);

		TAILQ_REMOVE(&(buddy.chunks[order].listHead)
			, firstPTR, entries);
		free(firstPTR);

		TAILQ_REMOVE(&(buddy.chunks[order].listHead)
			, secondPTR, entries);
		free(secondPTR);

		free_pages(merged->start, merged->order);
	}

	/**
	 * 5.	(@order + 1)-리스트를 기준으로, 작업 1 반복.
	 * 		(단, starting number은 Merged page의 starting number.)
	 */
}


/**
 * Print out the order-@order free chunk list
 *
 *  In the example above, print_free_pages(0) will print out:
 *  0x1d:0
 *
 *  print_free_pages(2):
 *    0x10:2
 *    0x18:2
 */
void print_free_pages(const unsigned int order)
{
	/**
	 * Your implementation should print out each free chunk from the beginning
	 * in the following format.
	 * WARNING: USE fprintf(stderr) NOT printf, otherwise the grading
	 * system will evaluate your implementation wrong.
	 */
	struct chunk *ptr = TAILQ_FIRST(&(buddy.chunks[order].listHead));
	while(ptr != NULL){
		if (ptr->isAlloc == FALSE){
			fprintf(stderr, "    0x%x:%u\n", ptr->start, ptr->order);
		}
		ptr = ptr->entries.tqe_next;
	}
}
/**
 * MAX_ORDER |
 *    ...    |
 *     4     | 0x00
 *     3     |
 *     2     | 0x10, 0x18
 *     1     | 0x1e
 *     0     | 0x1d
 */

/**
 * Return the unusable index(UI) of order-@order.
 *
 * Description:
 *    Return the unusable index of @order. In the above example, we have 27 free
 *  pages;
 *  # of free pages =
 *    sum(i = 0 to @MAX_ORDER){ (1 << i) * # of order-i free chunks }
 *
 *    and
 *
 *  UI(0) = 0 / 27 = 0.0 (UI of 0 is always 0 in fact).
 *  UI(1) = 1 (for 0x1d) / 27 = 0.037
 *  UI(2) = (1 (0x1d) + 2 (0x1e-0x1f)) / 27 = 0.111
 *  UI(3) = (1 (0x1d) + 2 (0x1e-0x1f) + 4 (0x10-0x13) + 4 (0x18-0x1b)) / 27
 *        = 0.407
 *  ...
 */
/**
 * 1st, Find # of free pages smaller than given order.
 * 2nd, Find # of total free pages.
 */
double get_unusable_index(unsigned int order)
{
	int arr[128] = { 0, };
	for (int i = 0; i <= MAX_ORDER; i++){
		int cur = 0;
		struct chunk *ptr = TAILQ_FIRST(&(buddy.chunks[i].listHead));
		while (ptr != NULL){
			if (ptr->isAlloc == FALSE){
				cur++;
			}
			ptr = ptr->entries.tqe_next;
		}
		arr[i] = (1 << i) * cur;
	}
	
	int sumTotal = 0;
	for (int i = 0; i <= MAX_ORDER; i++){
		sumTotal = sumTotal + arr[i];
	}

	int sumOrder = 0;
	for (int i = 0; i < order; i++){
		sumOrder = sumOrder + arr[i];
	}

	double retVal = (double)sumOrder / (double)sumTotal;
	return retVal;
}


/**
 * Initialize your buddy system.
 *
 * @nr_pages_in_order: number of pages in order-n notation to manage.
 * For instance, if @nr_pages_in_order = 13, the system should be able to
 * manage 8192 pages. You can set @nr_pages_in_order by using -n option while
 * launching the program;
 * ./pa4 -n 13       <-- will initiate the system with 2^13 pages.
 *
 * Return:
 *   0      : On successful initialization
 *  -EINVAL : Invalid arguments or when something goes wrong
 */
int init_buddy(unsigned int nr_pages_in_order)
{
	int i;

	/* TODO: Do your initialization as you need */
	fprintf(stdout, "MAX_ORDER: %d\tNR_ORDERS: %d\n", MAX_ORDER, NR_ORDERS);
	for (i = 0; i < NR_ORDERS; i++) {
		buddy.chunks[i].order = i;
		TAILQ_INIT(&(buddy.chunks[i].listHead));
	}

	/**
	 * TODO: Don't forget to initiate the free chunk list with
	 * order-@MAX_ORDER chunks. Note you might add multiple chunks if
	 * @nr_pages_in_order > @MAX_ORDER. For instance, when
	 * @nr_pages_in_order = 10 and @MAX_ORDER = 9, the initial free chunk
	 * lists will have two chunks; 0x0:9, 0x200:9.
	 */
	if (nr_pages_in_order > MAX_ORDER){
		/* Abnormal CASE: nr_pages_in_order > MAX_ORDER. */
		int startSum = 0;
		for(int count = 0; count < (1 << (nr_pages_in_order - MAX_ORDER)); count++){
			struct chunk *new = (struct chunk *)malloc(sizeof(struct chunk));
			new->start = startSum;
			new->order = MAX_ORDER;
			new->isAlloc = FALSE;
            startSum = startSum + (1 << MAX_ORDER);
			TAILQ_INSERT_TAIL(&(buddy.chunks[MAX_ORDER].listHead), new, entries);
		}
	}
	else{
		/* Normal CASE: nr_pages_in_order <= MAX_ORDER. */
		struct chunk *new = (struct chunk *)malloc(sizeof(struct chunk));
		new->start = 0;
		new->order = nr_pages_in_order;
		new->isAlloc = FALSE;
		TAILQ_INSERT_TAIL(&(buddy.chunks[nr_pages_in_order].listHead), new, entries);
	}

	return 0;
}


/**
 * Return resources that your buddy system has been allocated. No other
 * function will be called after calling this function.
 */
void fini_buddy(void)
{
	/**
	 * TODO: Do your finalization if needed, and don't forget to release
	 * the initial chunks that you put in init_buddy().
	 */
	struct chunk *freeptr;
	for(int i = MAX_ORDER; i >= 0; i--){
		while(!TAILQ_EMPTY(&(buddy.chunks[i].listHead))){
			freeptr = TAILQ_FIRST(&(buddy.chunks[i].listHead));
			TAILQ_REMOVE(&(buddy.chunks[i].listHead), freeptr, entries);
			free(freeptr);
		}
	}
}
